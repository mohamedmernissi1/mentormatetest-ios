//
//  URLConstants.swift
//  MentorMateTest
//
//  Created by mohamed mernissi on 18/6/2022.
//


import Foundation
struct APPURL {
    
    private struct Domains {
        static let BaseURL = "https://api.foursquare.com/v3"
    }

    private struct Routes {
        static let PlacesApi = "/places/"
    }

    static var BaseURL: String {
        return Domains.BaseURL + Routes.PlacesApi
    }
    
    static var search: String {
        return BaseURL  + "search"
    }

    static var APIKEY: String {
        return "fsq3WTkamwBvvCJKyrs1xGitkfQmOauoPb71TLciyyFqkiU="
    }
}
