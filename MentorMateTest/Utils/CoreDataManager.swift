//
//  CoreDataManager.swift
//  MentorMateTest
//
//  Created by mohamed mernissi on 18/6/2022.
//

import Foundation
import CoreData
import UIKit

class CoreDataManager {

    static let sharedManager = CoreDataManager()
    private let entityName = "Venue"

    private init() {} // Prevent clients from creating another instance.

    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "MentorMateTest")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in

            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    //3
    func saveContext () {
        let context = CoreDataManager.sharedManager.persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    func insertVenue(venuesReponse: [Result]){
        let managedContext = CoreDataManager.sharedManager.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: entityName,
                                                in: managedContext)!
        for item in venuesReponse {
            guard !venueExist(id: item.fsqID, managedContext: managedContext) else {
                continue
            }
            let venue = NSManagedObject(entity: entity, insertInto: managedContext)
            venue.setValue(item.fsqID, forKeyPath: "id")
            venue.setValue(item.name, forKeyPath: "name")
            do {
                try managedContext.save()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    }

    func venueExist(id: String, managedContext: NSManagedObjectContext) -> Bool {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.predicate = NSPredicate(format: "id == %@", id)
        let res = try! managedContext.fetch(fetchRequest)
        return res.count > 0 ? true : false
    }


    func fetchAllVenues() -> [VenuesListEntity]?{
        let managedContext = CoreDataManager.sharedManager.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entityName)
        do {
            let venues = try managedContext.fetch(fetchRequest)
            let venuesEntity = venues.map({
                VenuesListEntity(name: $0.value(forKeyPath: "name") as? String ?? "")
            })
            return venuesEntity
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            return nil
        }
    }
}

