//
//  ViewController.swift
//  MentorMateTest
//
//  Created by mohamed mernissi on 15/6/2022.
//

import UIKit

class ViewController: UIViewController {

    // MARK: - Variables

    private lazy var venuesListViewController: VenuesListView = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "VenuesListView", bundle: nil)
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "VenuesListView") as! VenuesListView
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        return viewController
    }()

    private lazy var aboutViewController: AboutView = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "AboutView", bundle: nil)
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "AboutView") as! AboutView
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        return viewController
    }()

    // MARK: - IBOutlets

    private lazy var segmentedControl: UISegmentedControl = {
        var segmentedControl = UISegmentedControl()
        // Configure Segmented Control
        segmentedControl.removeAllSegments()
        segmentedControl.insertSegment(withTitle: "Venues", at: 0, animated: false)
        segmentedControl.insertSegment(withTitle: "About", at: 1, animated: false)
        segmentedControl.addTarget(self, action: #selector(selectionDidChange(_:)), for: .valueChanged)

        // Select First Segment
        segmentedControl.selectedSegmentIndex = 0
        return segmentedControl
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupView()
    }

    private func setupView() {
        setupSegmentedControl()
        updateView()
    }

    private func setupSegmentedControl() {
        navigationItem.titleView = segmentedControl
    }

    @objc func selectionDidChange(_ sender: UISegmentedControl) {
        updateView()
    }

    private func updateView() {
        if segmentedControl.selectedSegmentIndex == 0 {
            remove(asChildViewController: aboutViewController)
            add(asChildViewController: venuesListViewController)
        } else {
            remove(asChildViewController: venuesListViewController)
            add(asChildViewController: aboutViewController)
        }
    }

    private func add(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChild(viewController)

        // Add Child View as Subview
        view.addSubview(viewController.view)

        // Configure Child View
        viewController.view.frame = view.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        // Notify Child View Controller
        viewController.didMove(toParent: self)
    }

    private func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParent: nil)

        // Remove Child View From Superview
        viewController.view.removeFromSuperview()

        // Notify Child View Controller
        viewController.removeFromParent()
    }

}

