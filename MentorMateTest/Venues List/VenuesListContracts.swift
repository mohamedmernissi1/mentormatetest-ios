//
//  VenuesListContracts.swift
//  MentorMateTest
//
//  Created mohamed mernissi on 15/6/2022.
//  Copyright © 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

//MARK: View -
/// VenuesList Module View Protocol
protocol VenuesListViewProtocol: AnyObject {
    // Update UI with value returned.
    func reloadTableView()
}

//MARK: Interactor -
/// VenuesList Module Interactor Protocol
protocol VenuesListInteractorProtocol {
    // Fetch Object from Data Layer
    func getVenuesApi(for presenter: VenuesListPresenterProtocol, latitude: String, longitude: String, limit: String, radius: String)
    func saveVenuesDatabase(venuesReponse: [Result], for presenter: VenuesListPresenterProtocol)
    func fetchVenuesDatabase(for presenter: VenuesListPresenterProtocol)
}

//MARK: Presenter -
/// VenuesList Module Presenter Protocol
protocol VenuesListPresenterProtocol {
    func getVenuesApi(latitude: Double, longitude: Double, limit: Int, radius: Int)
    func didGetVenuesApi(results: [Result])
    func failedToGetVenuesApi(error: String)
    func didFetch(with object: [VenuesListEntity])
    func didFailToFetch(with error: String)
    func didSaveVenues()
    func getVenueEntity(index: Int) -> VenuesListEntity
    func getVenuesCount() -> Int
}

//MARK: Router (aka: Wireframe) -
/// VenuesList Module Router Protocol
protocol VenuesListRouterProtocol {
}
