//
//  VenuesListPresenter.swift
//  MentorMateTest
//
//  Created mohamed mernissi on 15/6/2022.
//  Copyright © 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import CoreData

/// VenuesList Module Presenter
class VenuesListPresenter {
    
    weak private var _view: VenuesListViewProtocol?
    private var interactor: VenuesListInteractorProtocol
    private var wireframe: VenuesListRouterProtocol

    var venues = [VenuesListEntity]()

    
    init(view: VenuesListViewProtocol) {
        self._view = view
        self.interactor = VenuesListInteractor()
        self.wireframe = VenuesListRouter()
    }
}

// MARK: - extending VenuesListPresenter to implement it's protocol
extension VenuesListPresenter: VenuesListPresenterProtocol {

    func getVenuesApi(latitude: Double, longitude: Double, limit: Int, radius: Int) {
        print(#function)
        interactor.getVenuesApi(for: self, latitude: String(latitude),
                                longitude: String(longitude),
                                limit: String(limit),
                                radius: String(radius))
    }
    
    func didGetVenuesApi(results: [Result]) {
        print(#function)
        self.interactor.saveVenuesDatabase(venuesReponse: results,
                                           for: self)
    }

    func failedToGetVenuesApi(error: String) {
        print("Error:", error)
        self.interactor.fetchVenuesDatabase(for: self)
    }

    func didSaveVenues() {
        print(#function)
        self.interactor.fetchVenuesDatabase(for: self)
    }

    func didFetch(with object: [VenuesListEntity]) {
        print(#function)
        venues = object
        self._view?.reloadTableView()
    }

    func didFailToFetch(with error: String) {
        print("Error fetching:", error)
    }

    func getVenueEntity(index: Int) -> VenuesListEntity {
        return self.venues[index]
    }

    func getVenuesCount() -> Int {
        return self.venues.count
    }
}
