//
//  VenueCell.swift
//  MentorMateTest
//
//  Created by mohamed mernissi on 17/6/2022.
//

import UIKit

class VenueCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func fill(with object: VenuesListEntity) {
        lblName.text = object.name
    }

}
