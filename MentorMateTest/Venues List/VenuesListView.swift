//
//  VenuesListView.swift
//  MentorMateTest
//
//  Created mohamed mernissi on 15/6/2022.
//  Copyright © 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import CoreLocation

/// VenuesList Module View
class VenuesListView: UIViewController {

    // MARK: - Variables
    private var presenter: VenuesListPresenterProtocol!
    private var locationCoordinates: CLLocationCoordinate2D?

    // MARK: - Constants
    private let locationManager = CLLocationManager()

    // MARK: - @IBOutlets
    @IBOutlet weak var tableView: UITableView!

    // MARK: - Lifecycle methods

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = VenuesListPresenter(view: self)
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
}

// MARK: - extending VenuesListView to implement UITableView protocol
extension VenuesListView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getVenuesCount()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let venueCell = tableView.dequeueReusableCell(withIdentifier: "VenueCell", for: indexPath) as! VenueCell
        venueCell.fill(with: presenter.getVenueEntity(index: indexPath.row))
        return venueCell
    }
}

// MARK: - extending VenuesListView to implement CLLocationManager protocol
extension VenuesListView: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard locationCoordinates == nil else { return }
        locationCoordinates = manager.location?.coordinate
        presenter.getVenuesApi(latitude: locationCoordinates?.latitude ?? 0.0,
                               longitude: locationCoordinates?.longitude ?? 0.0,
                               limit: 5,
                               radius: 1000)
    }
}


// MARK: - extending VenuesListView to implement it's protocol
extension VenuesListView: VenuesListViewProtocol {
    func reloadTableView() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
