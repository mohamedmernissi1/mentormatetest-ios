//
//  VenuesListInteractor.swift
//  MentorMateTest
//
//  Created mohamed mernissi on 15/6/2022.
//  Copyright © 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import CoreData

/// VenuesList Module Interactor
class VenuesListInteractor: VenuesListInteractorProtocol {

    // MARK: - Variables
    private var venues: [NSManagedObject] = []

    // MARK: - Constants
    private let entityName = "Venue"

    func getVenuesApi(for presenter: VenuesListPresenterProtocol, latitude: String, longitude: String, limit: String, radius: String) {
        let headers = [
            "Accept": "application/json",
            "Authorization": APPURL.APIKEY
        ]
        var components = URLComponents(string: APPURL.search)!

        let coordinates = latitude + "," + longitude
        components.queryItems = [
            URLQueryItem(name: "ll", value: coordinates),
            URLQueryItem(name: "radius", value: radius),
            URLQueryItem(name: "limit", value: limit)
        ]
        components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
        components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: ",", with: "%2C")
        let request = NSMutableURLRequest(url: components.url!,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if let error = error {
                presenter.failedToGetVenuesApi(error: error.localizedDescription)
            } else {
                guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 else {
                    presenter.failedToGetVenuesApi(error: "Error: invalid HTTP response code ")
                    return
                }
                guard let data = data else {
                    presenter.failedToGetVenuesApi(error: "Error: missing data")
                    return
                }
                do {
                    let decoder = JSONDecoder()
                    let venues = try decoder.decode(VenuesResponse.self, from: data)

                    print("venues: ", venues.results.map({
                        $0.name
                    }), venues.results.count)
                    presenter.didGetVenuesApi(results: venues.results)
                }
                catch {
                    presenter.failedToGetVenuesApi(error: error.localizedDescription)
                }
            }
        })
        dataTask.resume()
    }
    
    func saveVenuesDatabase(venuesReponse: [Result], for presenter: VenuesListPresenterProtocol) {
        CoreDataManager.sharedManager.insertVenue(venuesReponse: venuesReponse)
        if let venues = CoreDataManager.sharedManager.fetchAllVenues() {
            if venues.count == venuesReponse.count {
                presenter.didSaveVenues()
            }
        }
    }

    func fetchVenuesDatabase(for presenter: VenuesListPresenterProtocol) {
        if let venues = CoreDataManager.sharedManager.fetchAllVenues() {
            presenter.didFetch(with: venues)
        } else {
            presenter.didFailToFetch(with: "Could not fetch data.")
        }
    }

    func venueExist(id: String, managedContext: NSManagedObjectContext) -> Bool {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.predicate = NSPredicate(format: "id == %@", id)
        let res = try! managedContext.fetch(fetchRequest)
        return res.count > 0 ? true : false
    }

}
