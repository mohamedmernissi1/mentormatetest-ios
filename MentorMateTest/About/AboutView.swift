//
//  AboutView.swift
//  MentorMateTest
//
//  Created mohamed mernissi on 15/6/2022.
//  Copyright © 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

/// About Module View
class AboutView: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Informs the Presenter that the View is ready to receive data.
    }
}
